package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"

	GS "gitlab.com/medblocks/medblocks_backend/globalStuff"
	"gitlab.com/medblocks/medblocks_backend/medIota"
)

func synchronizeToIota() {
	if len(medblockList) != 0 {
		sent, err := medIota.CreateAndSendMedblocks(medblockList)
		sentTransactions = append(sentTransactions, sent...)
		if err != nil {
			fmt.Println("SYNCHRONIZER: medblock transmit error: ", err)
			medblockList = nil
			sentTransactions = nil
			return
		}
		filter, _ := json.Marshal(sentTransactions)

		http.Post("http://localhost:7070/"+GS.ZMQListenerEndpoint+"/addTransactionFilter", "json/text", bytes.NewBuffer(filter))

		medblockList = nil
		sentTransactions = nil
	}
}
