package main

import (
	"encoding/json"
	"fmt"
	"net/http"

	GS "gitlab.com/medblocks/medblocks_backend/globalStuff"
	"gitlab.com/medblocks/medblocks_backend/go-couchdb"
	meddb "gitlab.com/medblocks/medblocks_backend/go-couchdb"
	mediota "gitlab.com/medblocks/medblocks_backend/medIota"
)

func enableCors(w http.ResponseWriter) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
}
func renderError(w http.ResponseWriter, message string, statusCode int) {
	w.WriteHeader(statusCode)
	fmt.Println(message)
	w.Write([]byte(message))
}
func userRequestInit(w http.ResponseWriter, r *http.Request) (GS.User, *couchdb.Client, error) {
	decoder := json.NewDecoder((*r).Body)
	var req GS.User
	err := decoder.Decode(&req)
	if err != nil {
		renderError(w, fmt.Sprintln("REQUEST_HANDLER: JSON user decode error: ", err), http.StatusBadRequest)
		return req, nil, err
	}
	clie, err := meddb.DBLogin("", "")
	if err != nil {
		renderError(w, fmt.Sprintln("REQUEST_HANDLER: DB client error: ", err), http.StatusInternalServerError)
		return req, nil, err
	}
	return req, clie, nil
}
func publicKeyRequestInit(w http.ResponseWriter, r *http.Request) (GS.User, *couchdb.Client, error) {
	decoder := json.NewDecoder((*r).Body)
	var req GS.User
	err := decoder.Decode(&req)
	if err != nil {
		renderError(w, fmt.Sprintln("REQUEST_HANDLER: JSON user decode error: ", err), http.StatusBadRequest)
	}
	clie, err := meddb.DBLogin("", "")
	if err != nil {
		renderError(w, fmt.Sprintln("REQUEST_HANDLER: DB client error: ", err), http.StatusInternalServerError)
		return req, nil, err
	}
	return req, clie, nil
}

func fileRequestInit(w http.ResponseWriter, r *http.Request) (GS.File, *couchdb.Client, bool) {
	decoder := json.NewDecoder((*r).Body)
	var req GS.File
	clie, err := meddb.DBLogin("", "")
	dBClient = clie
	if err != nil {
		renderError(w, fmt.Sprintln("REQUEST_HANDLER: DB client error: ", err), http.StatusInternalServerError)
		return req, nil, false
	}
	err = decoder.Decode(&req)
	if err != nil {
		renderError(w, fmt.Sprintln("REQUEST_HANDLER: JSON file decode error: ", err), http.StatusBadRequest)
		return req, nil, false
	}
	if req.OwnerEmailID == "" || req.SenderEmailID == "" || req.Signature == "" || req.IPFSHash == "" || req.AESKey == "" {
		renderError(w, fmt.Sprintln("REQUEST_HANDLER: Invalid file upload request"), http.StatusBadRequest)
		return req, nil, false
	}

	ownerExists, err := checkUserExists(req.OwnerEmailID)
	if err != nil {
		renderError(w, fmt.Sprintln("REQUEST_HANDLER: Owner check error: ", err), http.StatusInternalServerError)
		return req, nil, false
	}
	if !ownerExists {
		renderError(w, fmt.Sprintln("REQUEST_HANDLER: Owner not found: ", req.OwnerEmailID), http.StatusNotFound)
		return req, nil, false
	}

	senderExists, err := checkUserExists(req.SenderEmailID)
	if err != nil {
		renderError(w, fmt.Sprintln("REQUEST_HANDLER: Sender check error: ", err), http.StatusInternalServerError)
		return req, nil, false
	}
	if !senderExists {
		renderError(w, fmt.Sprintln("REQUEST_HANDLER: Sender not found: ", req.SenderEmailID), http.StatusNotFound)
		return req, nil, false
	}
	return req, clie, true
}

func checkUserExists(emailID string) (bool, error) {
	dbExists, _, err := dBClient.GetUser(emailID)
	if err != nil {
		fmt.Println("REQUEST_HANDLER: DB user check error", err)
		return false, err
	}
	if dbExists {
		return true, nil
	} else {
		iotaExists, err := mediota.CheckUserExists(emailID)
		if err != nil {
			fmt.Println("REQUEST_HANDLER: iota user check error", err)
			return false, err
		}
		if iotaExists {
			return true, nil
		}
	}
	return false, nil
}
