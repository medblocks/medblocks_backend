package couchdb

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"

	GS "gitlab.com/medblocks/medblocks_backend/globalStuff"
)

//DBLogin user login.
func DBLogin(username, password string) (*Client, error) {
	//user, pass := flag.String("u", username, "username"), flag.String("p", password, "password")
	client, err := NewClient("http://"+GS.DatabaseEndpoint+":5984", nil)
	return client, err
}

//GetUser returns private and public key given the user emailid.
func (cl *Client) GetUser(emailid string) (bool, GS.User, error) {
	db, err := cl.EnsureDB("user")
	var user GS.User
	if err != nil {
		return false, user, err
	}
	path := revpath("", db.name, emailid)
	resp, err := db.request("GET", path, nil)
	if err != nil {
		if ErrorStatus(err, http.StatusNotFound) {
			return false, user, nil
		}
		return false, user, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return false, user, err
	}
	json.Unmarshal(body, &user)
	return true, user, nil
}

func (db *DB) addDoc(doc interface{}, key string) error {
	_, err := db.Put(key, doc, "")
	if err != nil {
		return err
	}
	return err
}

/*AddUser adds user entry to user db.
Document structure , doc := `{"emailid":"admin@mb.com","privatekey":"111","publickey":"000"}`*/
func (cl *Client) AddUser(doc GS.User) error {
	var db *DB
	var err error
	db, err = cl.EnsureDB("user")
	if err != nil {
		fmt.Println("hmm")
		return err
	}
	doc.EPrivateKey = ""
	doc.SPrivateKey = ""
	err = db.addDoc(doc, doc.EmailID)
	if err != nil {
		fmt.Println("hmm2")
	}
	return err
}

type FileEntry struct {
	ID        string `json:"_id"`
	Rev       string `json:"_rev"`
	IV        string `json:"iv"`
	Publickey string `json:"publickey"`
	IPFShash  string `json:"ipfshash"`
	Type      string `json:"type"`
	Size      string `json:"size"`
	FileData  []byte
}

type Val struct {
	Doc      []FileEntry `json:"docs"`
	Bookmark string      `json:"bookmark"`
	Warning  string      `json:"warning"`
}

type Sel struct {
	Row Row `json:"selector"`
}

type Row struct {
	Eid string `json:"publickey"`
}

/*AddFile adds file entry to file db . Should be called after file is added to IPFS.
Document structure , doc := `{"iv":"1111","publickey":"000","ipfshash":"qmsds","type":"jpg","size":11}`*/
func (cl *Client) AddFile(doc GS.File) error {
	var db *DB
	var err error
	db, err = cl.EnsureDB("file")
	if err != nil {
		db, _ = cl.CreateDB("file")
	}
	return db.addDoc(doc, doc.IPFSHash)
}

//EnsureFile ensures that the file exists and user has access to the file.
func (cl *Client) EnsureFile(ipfshash string, publickey string) (bool, error) {
	db, err := cl.EnsureDB("file")
	if err != nil {
		return false, err
	}
	path := revpath("", db.name, ipfshash)
	resp, err := db.request("GET", path, nil)
	if err != nil {
		return false, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return false, err
	}
	var couchresp FileEntry
	json.Unmarshal(body, &couchresp)
	if couchresp.Publickey == publickey {
		return true, nil
	}
	return false, errors.New("user doesn't have access")
}

//GetFileList returns an array of file entries which the user has access to.
func (cl *Client) GetFileList(publickey string) (bool, []GS.File, error) {
	db, err := cl.EnsureDB("file")
	if err != nil {
		return false, []GS.File{}, err
	}
	jsonstr := Sel{Row: Row{Eid: publickey}}
	data, _ := json.Marshal(jsonstr)
	path := revpath("", db.name)
	path = path + "/_find"
	resp, err := db.request("POST", path, bytes.NewReader(data))
	if err != nil {
		return false, []GS.File{}, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return false, []GS.File{}, err
	}
	var couchresp Val
	json.Unmarshal(body, &couchresp)
	docs := []GS.File{}
	for _, element := range couchresp.Doc {
		docs = append(docs, GS.File{element.IV, element.Publickey, element.IPFShash, element.Type, element.Size})
	}
	return false, docs, nil
}
