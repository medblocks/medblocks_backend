package main

import (
	"encoding/json"
	"fmt"

	GS "gitlab.com/medblocks/medblocks_backend/globalStuff"
	"gitlab.com/medblocks/medblocks_backend/medIota"
)

func zmqProcessRegister(medblock medIota.Medblock) {
	var data GS.User
	json.Unmarshal([]byte(medblock.Message), &data)
	if data.EmailID == "" || data.SPublicKey == "" || data.EPublicKey == "" {
		fmt.Println("SYNCHRONIZER: Incomplete medblock from ZMQ")
		return
	}
	fmt.Println("Registeration from IOTA: ", data.EmailID)
	err := clie.AddUser(data)
	if err != nil {
		fmt.Println("SYNCHRONIZER: DB add user error: ", err)
	}
}
