package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"net/http"

	"github.com/iotaledger/iota.go/transaction"
	GS "gitlab.com/medblocks/medblocks_backend/globalStuff"
)

func addTransactionFilter(transactionBuffer *atomicTransactionBuffer) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		var transactions transaction.Transactions
		err := json.NewDecoder(r.Body).Decode(&transactions)
		if err != nil {
			fmt.Println("ZMQ_LISTENER: filter transaction decode error: ", err)
			return
		}
		transactionBuffer.appendTransactions(transactions)
	}
}

func main() {
	var transactionBufferShared atomicTransactionBuffer
	docker := flag.Bool("docker", false, "pass to change endpoints to docker")
	flag.Parse()
	GS.SetEndpoints(docker)
	go zmqListener(&transactionBufferShared)
	http.HandleFunc("/zmqListener/addTransactionFilter", addTransactionFilter(&transactionBufferShared))
	http.ListenAndServe(":7070", nil)
}
