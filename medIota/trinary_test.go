package medIota

import (
	"math/rand"
	"testing"
	"time"
)

var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890{}[].,\"()'+-=")

func randSeq(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}

func TestConv(t *testing.T) {
	rand.Seed(time.Now().UnixNano())
	ta := 0
	for i := 0; i < 1000000; i++ {
		str := randSeq(rand.Intn(100) + 10)
		res, _ := stringToTrytes(str)
		res2, _ := trytesToString(res)
		// sz := len(str)
		// str = str[:sz-1]
		if str != res2 {
			t.Errorf("%d Incorrect, want: %s, got: %s.", ta, str, res2)
			ta = ta + 1
		}
	}
}
