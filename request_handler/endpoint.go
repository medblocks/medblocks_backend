package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net/http"

	GS "gitlab.com/medblocks/medblocks_backend/globalStuff"
	meddb "gitlab.com/medblocks/medblocks_backend/go-couchdb"
)

var dBClient *meddb.Client

func loginHandler(w http.ResponseWriter, r *http.Request) {
	enableCors(w)
	defer r.Body.Close()
	req, dBCl, err := userRequestInit(w, r)
	dBClient = dBCl
	if err != nil {
		return
	}
	if req.EmailID == "" {
		fmt.Println("REQUEST_HANDLER: EMPTY user email ID")
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	fmt.Println("Processing login request...", req.EmailID)

	loginUser(req.EmailID, w)
}

func userRegistrationHandler(w http.ResponseWriter, r *http.Request) {
	enableCors(w)
	defer r.Body.Close()
	req, dBCl, err := userRequestInit(w, r)
	dBClient = dBCl
	if err != nil {
		return
	}

	if req.EmailID == "" || req.SPrivateKey == "" || req.SPublicKey == "" || req.EPrivateKey == "" || req.EPublicKey == "" {
		fmt.Println("REQUEST_HANDLER: Invalid user registration request")
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	fmt.Println("Processing registration request...", req.EmailID)

	userExists, err := checkUserExists(req.EmailID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	if userExists {
		fmt.Println("Already Exists: ", req.EmailID)
		w.WriteHeader(http.StatusConflict)
		return
	}
	fmt.Println("Registration from request: ", req.EmailID)
	registerUser(req, w)
}

func getPublicKeyHandler(w http.ResponseWriter, r *http.Request) {
	enableCors(w)
	defer r.Body.Close()
	req, dBCl, err := publicKeyRequestInit(w, r)
	dBClient = dBCl
	if err != nil {
		return
	}
	if req.EmailID == "" {
		fmt.Println("REQUEST_HANDLER: EMPTY user email ID")
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	fmt.Println("Processing login request...", req.EmailID)
	getPublicKey(req.EmailID, w)
}
func filePutHandler(w http.ResponseWriter, r *http.Request) {
	enableCors(w)
	req, dbCl, success := fileRequestInit(w, r)
	dBClient = dbCl
	if !success {
		return
	}
	fmt.Println("Processing file put request: ", req.IPFSHash)
	w.WriteHeader(http.StatusAccepted)
}
func badGatewayHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusBadGateway)
}

func main() {
	docker := flag.Bool("docker", false, "pass to change endpoints to docker")
	flag.Parse()
	GS.SetEndpoints(docker)
	http.HandleFunc("/login", loginHandler)
	http.HandleFunc("/registerUser", userRegistrationHandler)
	http.HandleFunc("/getPublicKey", getPublicKeyHandler)
	http.HandleFunc("/postFile", filePutHandler)
	http.HandleFunc("/", badGatewayHandler)
	var test, test2 GS.File

	test2.AESKey = "adw"
	test2.IPFSHash = "awdwad"
	a, _ := json.Marshal(test2)
	fmt.Println(string(a), test)
	json.Unmarshal([]byte(`{"AES":"awd","SenderEmailID":"awd2","OE":"awdawd"}`), &test)
	fmt.Println(test)
	log.Fatal(http.ListenAndServe(":8080", nil))
}
