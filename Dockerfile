FROM medblocks_dev_base
ARG build_cmd
ARG workdir=/ 
ADD . /go/src/gitlab.com/medblocks/medblocks_backend/
WORKDIR $workdir
RUN $build_cmd
