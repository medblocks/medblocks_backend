package globalStuff

func SetEndpoints(docker *bool) {
	if *docker {
		SynchronizerEndpoint = "synchronizer"
		DatabaseEndpoint = "couchdb"
		ZMQListenerEndpoint = "zmqListener"
	}
}
