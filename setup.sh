#!/bin/sh
sudo docker build -f Dockerfile.base -t medblocks_dev_base .
sudo docker-compose build
sudo docker-compose up &
sleep 30

#initializing couchDB as single node
curl -X PUT http://localhost:5984/_users
curl -X PUT http://localhost:5984/_replicator

#adding databases to couchDB
curl -X PUT http://localhost:5984/users

sudo docker-compose down
