package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"

	GS "gitlab.com/medblocks/medblocks_backend/globalStuff"
)

func registerUserDb(req GS.User) error {
	err := dBClient.AddUser(req)
	if err != nil {
		fmt.Println("REQUEST_HANDLER: add user error: ", err)
		return err
	}
	return nil
}
func registerUserOracle(req GS.User) error {
	reqStr, _ := json.Marshal(req)
	//client := &http.Client{}
	//oracleReq, _ := http.NewRequest("POST", "https://dev.medblocks.org/api/verify/", bytes.NewBuffer([]byte(reqStr)))
	r, err := http.Post("https://dev.medblocks.org/api/verify/", "application/json", bytes.NewBuffer([]byte(reqStr)))
	//oracleReq.Header.Set("Content-Type", "application/json")
	//r, err := client.Do(oracleReq)
	if err != nil {
		fmt.Println("REQUEST_HANDLER: Oracle request error: ", err)
		return err
	}
	if r.StatusCode > 299 || r.StatusCode < 200 {
		e := errors.New("REQUEST_HANDLER: Oracle DataError")
		fmt.Println("REQUEST_HANDLER: Oracle DataError")
		return e
	}
	return nil
}
func registerUserSynchronizer(req GS.User) error {
	req.SPrivateKey = ""
	req.EPrivateKey = ""
	reqStr, _ := json.Marshal(req)
	syncRequest := GS.RequestWrapper{RequestType: GS.NewUser, RequestMessage: string(reqStr)}
	syncRequestString, _ := json.Marshal(syncRequest)
	newReq, _ := http.NewRequest("POST", "http://"+GS.SynchronizerEndpoint+":3000/synchronizer/addRequest", bytes.NewBuffer([]byte(syncRequestString)))
	newReq.Header.Set("Content-Type", "application/json")
	client := &http.Client{}
	_, err := client.Do(newReq)
	if err != nil {
		print("REQUEST_HANDLER: sync register error: ", err)
		return err

	}
	return nil
}
func registerUser(req GS.User, w http.ResponseWriter) {
	var err error

	// err = registerUserDb(req, dBClient)
	// if err != nil {
	// 	w.WriteHeader(http.StatusInternalServerError)
	// 	return
	// }

	err = registerUserOracle(req)
	if err != nil {
		if err.Error() == "REQUEST_HANDLER: Oracle DataError" {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	// err = registerUserSynchronizer(req)
	// if err != nil {
	// 	w.WriteHeader(http.StatusInternalServerError)
	// 	return
	// }

	w.WriteHeader(http.StatusAccepted)
}
