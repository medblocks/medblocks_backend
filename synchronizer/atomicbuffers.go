package main

import (
	"sync"

	. "gitlab.com/medblocks/medblocks_backend/globalStuff"
	"gitlab.com/medblocks/medblocks_backend/medIota"
)

type atomicMedblockBuffer struct {
	mu           sync.Mutex // A lock than can be held by one goroutine at a time.
	medblocks    []medIota.Medblock
	newMedblocks bool
}

func (buffer *atomicMedblockBuffer) appendMedblocks(inputMedblocks []medIota.Medblock) {
	buffer.mu.Lock()
	buffer.medblocks = append(buffer.medblocks, inputMedblocks...)
	buffer.newMedblocks = true
	buffer.mu.Unlock()
}

func (buffer *atomicMedblockBuffer) retrieveMedblocks(outputMedblocks *[]medIota.Medblock) {
	buffer.mu.Lock()
	*outputMedblocks = append(*outputMedblocks, buffer.medblocks...)
	buffer.newMedblocks = false
	buffer.medblocks = nil
	buffer.mu.Unlock()
}
func (buffer *atomicMedblockBuffer) checkNewMedblocks() bool {
	buffer.mu.Lock()
	output := buffer.newMedblocks
	buffer.mu.Unlock()
	return output
}

type atomicRequestBuffer struct {
	mu          sync.Mutex // A lock than can be held by one goroutine at a time.
	requests    []RequestWrapper
	newRequests bool
}

func (buffer *atomicRequestBuffer) appendRequest(inputRequests RequestWrapper) {
	buffer.mu.Lock()
	buffer.requests = append(buffer.requests, inputRequests)
	buffer.newRequests = true
	buffer.mu.Unlock()
}
func (buffer *atomicRequestBuffer) retrieveRequests(outputRequests *[]RequestWrapper) {
	buffer.mu.Lock()
	*outputRequests = append(*outputRequests, buffer.requests...)
	buffer.newRequests = false
	buffer.requests = nil
	buffer.mu.Unlock()
}
func (buffer *atomicRequestBuffer) checkNewRequests() bool {
	buffer.mu.Lock()
	output := buffer.newRequests
	buffer.mu.Unlock()
	return output
}
