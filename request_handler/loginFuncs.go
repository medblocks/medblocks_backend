package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	GS "gitlab.com/medblocks/medblocks_backend/globalStuff"
	mediota "gitlab.com/medblocks/medblocks_backend/medIota"
)

func getUserDB(userEmail string) (bool, GS.User, error) {
	userExists, user, err := dBClient.GetUser(userEmail)
	var resp GS.User
	if err != nil {
		fmt.Println("REQUEST_HANDLER: DB user get error: ", err)
		return false, resp, err
	}
	if userExists {
		var resp = GS.User{EmailID: user.EmailID, SPublicKey: user.SPublicKey, EPublicKey: user.EPublicKey}
		return true, resp, nil
	}
	return false, resp, nil
}

func getUserIota(userEmail string) (GS.User, error) {
	var resp GS.User
	userData, err := mediota.GetUserMedblocks(userEmail)
	if err != nil {
		fmt.Println("REQUEST_HANDLER: iota user get error: ", err)
		return resp, err
	}
	for _, medblock := range userData {
		switch medblock.Type {
		case mediota.REGISTER:
			json.Unmarshal([]byte(medblock.Message), &resp)
			err := registerUserDb(resp)
			if err != nil {
				fmt.Println("REQUEST_HANDLER: DB add user error: ", err)
				return resp, err
			}
		}
	}
	return resp, nil
}
func getUserOracle(userEmail string) (GS.User, error) {
	var resp GS.User
	oracleReq, _ := http.NewRequest("GET", "https://dev.medblocks.org/api/verify/"+userEmail+"/", nil)
	client := &http.Client{}
	oracleResp, err := client.Do(oracleReq)
	if err != nil {
		fmt.Println("REQUEST_HANDLER: Oracle user get error: ", err)
		return resp, err
	}
	userDataStr, _ := ioutil.ReadAll(oracleResp.Body) //mediota.GetUserMedblocks(req.EmailID)
	json.Unmarshal(userDataStr, &resp)
	return resp, nil
}
func completeUser(userPublic GS.User) (GS.User, error) {
	var resp GS.User
	userPrivate, err := getUserOracle(userPublic.EmailID)
	if err != nil {
		return resp, err
	}
	resp = GS.User{EmailID: userPublic.EmailID, SPublicKey: userPublic.SPublicKey, SPrivateKey: userPrivate.SPrivateKey, EPublicKey: userPublic.EPublicKey, EPrivateKey: userPrivate.EPrivateKey}
	return resp, nil
}
func loginUser(userEmail string, w http.ResponseWriter) {
	var userPublic GS.User
	dbUserExists, userPublic, err := getUserDB(userEmail)
	if dbUserExists {
		resp, err := completeUser(userPublic)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		respStr, _ := json.Marshal(resp)
		w.Write(respStr)
		w.WriteHeader(http.StatusAccepted)
		return
	}
	iotaUserExists, err := mediota.CheckUserExists(userEmail)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	if iotaUserExists {
		userPublic, err := getUserIota(userEmail)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		resp, err := completeUser(userPublic)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		respStr, _ := json.Marshal(resp)
		w.Write(respStr)
		w.WriteHeader(http.StatusAccepted)
		return
	}
	w.WriteHeader(http.StatusNotFound)
}

func getPublicKey(userEmail string, w http.ResponseWriter) {
	var resp GS.User
	dbUserExists, resp, err := getUserDB(userEmail)
	if dbUserExists {
		respStr, _ := json.Marshal(resp)
		w.Write(respStr)
		w.WriteHeader(http.StatusAccepted)
		return
	}
	iotaUserExists, err := mediota.CheckUserExists(userEmail)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	if iotaUserExists {
		resp, err := getUserIota(userEmail)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		respStr, _ := json.Marshal(resp)
		w.Write(respStr)
		w.WriteHeader(http.StatusAccepted)
		return
	}
	w.WriteHeader(http.StatusNotFound)
}
