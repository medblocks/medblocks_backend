package medIota

import (
	"strings"

	"github.com/iotaledger/iota.go/checksum"
	"github.com/iotaledger/iota.go/consts"
	"github.com/iotaledger/iota.go/trinary"
)

var endpoint = "http://18.216.126.204:14265"

var seed = trinary.Trytes("VGASBDYGWAIUGWADAWD9VGASBDYGWAIUGWADARWD9VGASBDYGWAIUGWADAWD9VGASBDYGWAIUGWADAWD9")

const mwm = 3
const depth = 3

var tagList = map[MedblockType]string{
	REGISTER:   "GFAQBESKMJIPYWPARQBZMROJVPP",
	BODY:       "M9CJ9DLLGBDI9ZPXRIIPDCEBWGO",
	KEY:        "GQAZH9JTKGRTKMWQSLSYSVQ9HJG",
	PERMISSION: "FKXHTC9ERWPKOXEBAFFYUTRDXJO",
	FILE:       "WYKOYVPPSGWVSPZIJXWHJTUEU9O",
	DENY:       "K9FZJKOSGDRNRYCTGOPWSDBGYAL"}

/*
 *  stringToTrytes: convert an ASCII encoded string to Trytes.
 *  Input:  An ASCII encoded string
 *  Output: A trinary.Tryte object
 *
 *      The function takes in a string with ASCII encoded characters and converts represents
 *  each ASCII character using a set of two letters from the Tryte alphabet (9ABCDEFGHIJKLMNOPQRSTUVWXYZ)
 *  Since the tryte alphabet consists of 27 symbols, the ASCII value is converted to base 27 digits. The
 *  least significant base 27 digit is represented first, followed by the most significant digit.
 */

func StringToTrytes(input string) (trinary.Trytes, error) {
	tryteString := []rune{}
	tryteAlphabet := []rune(consts.TryteAlphabet)
	for _, char := range []rune(input) {
		ascii := int(char)
		ones := ascii % 27
		threes := (ascii - ones) / 27
		tryteString = append(tryteString, tryteAlphabet[ones])
		tryteString = append(tryteString, tryteAlphabet[threes])
	}
	return trinary.NewTrytes(string(tryteString))
}

/*
 * trytesToString: Converts a trinary.Trytes object to an ASCII encoded string.
 *  This function simply reverses the process used by StringToTrytes to give back the ASCII encoded string
 */

func TrytesToString(input trinary.Trytes) (string, error) {
	err := trinary.ValidTrytes(input)
	if err != nil {
		return input, err
	}

	tryteRunes := []rune(input)
	strRunes := []rune{}
	alphabetMap := make(map[rune]int)

	for num, char := range []rune(consts.TryteAlphabet) {
		alphabetMap[char] = num
	}

	for i := 0; i < len(tryteRunes)-1; i += 2 {
		j := alphabetMap[tryteRunes[i]]
		j += alphabetMap[tryteRunes[i+1]] * 27
		strRunes = append(strRunes, rune(j))
	}
	str := strings.Trim(string(strRunes), string(0))
	return str, err
}

func EmailToAddress(email string) (trinary.Trytes, error) {
	intermediate, err := StringToTrytes(email)
	if err != nil {
		return "", err
	}
	if len(intermediate) < 81 {
		for i := 81 - len(intermediate); i > 0; i-- {
			intermediate += "9"
		}
	}
	final, err := checksum.AddChecksum(string(intermediate), true, 9)
	return final, err
}
