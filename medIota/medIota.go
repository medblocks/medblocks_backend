package medIota

import (
	"crypto"
	"crypto/rsa"
	"crypto/sha256"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"math/big"

	. "github.com/iotaledger/iota.go/api"
	"github.com/iotaledger/iota.go/bundle"
	"github.com/iotaledger/iota.go/pow"
	"github.com/iotaledger/iota.go/transaction"
)

type Medblock struct {
	Message        string       `json:"data,omitempty"`
	Signature      string       `json:"signature,omitempty"`
	Type           MedblockType `json:"type,omitempty"`
	RecipientEmail string       `json:"recepientEmail,omitempty"`
}

//MedblockType Used to define transaction type and fetch tag used for the transaction
type MedblockType int

const (
	//REGISTER Transaction type for user registration
	REGISTER MedblockType = 0
	//BODY ...
	BODY MedblockType = 1
	//KEY ...
	KEY MedblockType = 2
	//PERMISSION Transaction type used to indicate request for permission for a file
	PERMISSION MedblockType = 3
	//FILE Transaction type used to add permission for a file to a user
	FILE MedblockType = 4
	//DENY Transaction type used to indicate denial of permission request
	DENY MedblockType = 5
)

func GetAPI() (*API, error) {
	return ComposeAPI(HTTPClientSettings{
		URI:                  endpoint,
		LocalProofOfWorkFunc: pow.SyncGoProofOfWork,
	})
}

//CreateAndSendMedblocks Creates and broadcasts a bundle of medblocks made from data provided
func CreateAndSendMedblocks(medblockList []Medblock) (transaction.Transactions, error) {
	prepTransferOpts := PrepareTransfersOptions{}
	var transfers bundle.Transfers
	var err error
	api, err := GetAPI()
	if err != nil {
		return nil, err
	}
	var tempBlock Medblock
	for _, block := range medblockList {

		add, err := EmailToAddress(block.RecipientEmail)
		if err != nil {
			fmt.Println("MEDIOTA: trinary address conversion error[", block.RecipientEmail, "]: ", err)
			continue
		}
		tempBlock.Message = block.Message
		tempBlock.Signature = block.Signature
		msg, _ := json.Marshal(tempBlock)
		message, err := StringToTrytes(string(msg))
		if err != nil {
			fmt.Println("MEDIOTA: trinary message conversion error[", msg, "]: ", err)
			continue
		}
		transfers = append(transfers, bundle.Transfer{
			Address: add,
			Value:   0,
			Tag:     tagList[block.Type],
			Message: message})
	}
	trytes, err := api.PrepareTransfers(seed, transfers, prepTransferOpts)
	if err != nil {
		return nil, err
	}
	bndl, _ := api.SendTrytes(trytes, depth, mwm)
	if err != nil {
		return nil, err
	}
	return bndl, nil
}

func FilterConfirmedHashes(transactionHashes []string, api *API) (confirmedTransactions []string, err error) {
	confirmations, err := api.GetLatestInclusion(transactionHashes)
	if err != nil {
		return nil, err
	}
	for index, isConfirmed := range confirmations {
		if isConfirmed == true {
			confirmedTransactions = append(confirmedTransactions, transactionHashes[index])
		}
	}
	return confirmedTransactions, nil
}

func FilterConfirmedTransactions(transactionObjects transaction.Transactions, api *API) (confirmedTransactions transaction.Transactions, err error) {
	var transactionHashes []string
	for _, tx := range transactionObjects {
		transactionHashes = append(transactionHashes, tx.Hash)
	}
	confirmations, err := api.GetLatestInclusion(transactionHashes)
	if err != nil {
		return nil, err
	}
	for index, isConfirmed := range confirmations {
		if isConfirmed == true {
			confirmedTransactions = append(confirmedTransactions, transactionObjects[index])
		}
	}
	return confirmedTransactions, nil
}

func FilterAndClassifyMedblocks(transactionObjects transaction.Transactions) (medblocks []Medblock) {
	for _, tx := range transactionObjects {
		var temp Medblock
		msg, _ := TrytesToString(tx.SignatureMessageFragment)
		for txType, tag := range tagList {
			if tx.Tag == tag {
				json.Unmarshal([]byte(msg), &temp)
				temp.Type = txType
				temp.RecipientEmail, _ = TrytesToString(tx.Address)
				if ValidateMedblock(temp) {
					medblocks = append(medblocks, temp)
				}
				break
			}
		}

	}
	return medblocks
}

//CheckUserExists returns if user with given EmailID exists on the medblock network
func CheckUserExists(userEmail string) (bool, error) {
	api, err := GetAPI()
	if err != nil {
		return false, err
	}
	var queryAddress, queryTag []string
	add, err := EmailToAddress(userEmail)
	if err != nil {
		return false, err
	}
	queryAddress = append(queryAddress, add)
	queryTag = append(queryTag, tagList[REGISTER])
	results, err := api.FindTransactions(FindTransactionsQuery{Addresses: queryAddress, Tags: queryTag})
	//results = FilterConfirmedHashes(results, api)
	if err != nil {
		return false, err
	}
	if len(results) != 0 {
		return true, nil
	}
	return false, nil
}

//GetUserMedblocks Returns messages types of all medblocks sent to a user on the network
func GetUserMedblocks(userEmail string) (medblocks []Medblock, err error) {
	api, _ := GetAPI()
	var getAddress []string
	add, err := EmailToAddress(userEmail)
	if err != nil {
		return nil, err
	}
	getAddress = append(getAddress, add)
	candidateTransactions, err := api.FindTransactionObjects(FindTransactionsQuery{Addresses: getAddress})
	if err != nil {
		return nil, err
	}
	if err != nil {
		return nil, err
	}

	return FilterAndClassifyMedblocks(candidateTransactions), nil
}

func GetOraclePublicKey() rsa.PublicKey {
	var a rsa.PublicKey
	a.N = big.NewInt(0)
	a.N.SetString("151553962689194941850752026531724878809801912270964480521114579725998061279390585191411615556927136399752543110328948393847650390483209379108519705985110099469285545721669707252451467223234315501004366436435904226325925468254461153006262591303165748286326664548729411829777485627055603823048382037025809681083", 10)
	a.E = 65537
	return a
}
func ValidateMedblock(block Medblock) bool {
	switch block.Type {
	case REGISTER:
		fmt.Println("register medblock")
		publicKey := GetOraclePublicKey()
		hasher := sha256.New()
		hasher.Write([]byte(block.Message))
		hashed := hasher.Sum(nil)
		sign, _ := base64.StdEncoding.DecodeString(block.Signature)
		if rsa.VerifyPKCS1v15(&publicKey, crypto.SHA256, hashed, sign) != nil {
			fmt.Println("Could not validate block")
			return false
		}
		return true
		break
	}
	return true
}
