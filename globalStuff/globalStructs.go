package globalStuff

import "gitlab.com/medblocks/medblocks_backend/medIota"

type RequestType int

var SynchronizerEndpoint string = "localhost"
var ZMQListenerEndpoint string = "localhost"
var DatabaseEndpoint string = "localhost"

const (
	Invalid RequestType = 0
	NewUser RequestType = 1
)

type ZMQRequest struct {
	Medblocks []medIota.Medblock `json:"Medblocks"`
}

//HandlerRequest  format of request expected to addRequest
type RequestWrapper struct {
	RequestType    RequestType `json:"RequestType"`    //Type type of request
	RequestMessage string      `json:"RequestMessage"` //Message JSON formatted as one of the below inputs
}

type User struct {
	EmailID     string `json:"emailId,omitempty"`
	EPublicKey  string `json:"ePublicKey,omitempty"`
	EPrivateKey string `json:"ePrivateKey,omitempty"`
	SPublicKey  string `json:"sPublicKey,omitempty"`
	SPrivateKey string `json:"sPrivateKey,omitempty"`
}

type File struct {
	IPFSHash      string `json:"IH,omitempty"`
	SenderEmailID string `json:"SE,omitempty"`
	OwnerEmailID  string `json:"OE,omitempty"`
	AESKey        string `json:"AES,omitempty"`
	Signature     string `json:"SG,omitempty"`
}

type Permission struct {
	IPFSHash        string `json:"IH,omitempty"`
	SenderEmailID   string `json:"SE,omitempty"`
	RecieverEmailID string `json:"RE,omitempty"`
	OwnerEmailID    string `json:"OE,omitempty"`
	AESKey          string `json:"AES,omitempty"`
	Signature       string `json:"SG,omitempty"`
}
