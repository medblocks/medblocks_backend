package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"time"

	. "github.com/iotaledger/iota.go/api"
	"github.com/iotaledger/iota.go/transaction"
	"github.com/iotaledger/iota.go/trinary"
	zmq "github.com/pebbe/zmq4"
	GS "gitlab.com/medblocks/medblocks_backend/globalStuff"
	"gitlab.com/medblocks/medblocks_backend/medIota"
)

var context *zmq.Context
var socket *zmq.Socket
var api *API
var txHash trinary.Hash
var transactions, filteredTransactions transaction.Transactions
var medblocks []medIota.Medblock

func zmqInit() error {
	context, _ = zmq.NewContext()
	socket, _ = context.NewSocket(zmq.SUB)
	err := socket.Connect("tcp://18.216.126.204:5556")
	if err != nil {
		fmt.Println("ZMQ_LISTENER: Socket connect error: ", err)
		return err
	}
	err = socket.SetSubscribe("tx")
	if err != nil {
		fmt.Println("ZMQ_LISTENER: Socket subscribe error: ", err)
		return err
	}
	api, err = medIota.GetAPI()
	if err != nil {
		fmt.Println("ZMQ_LISTENER: get API error: ", err)
		return err
	}
	return nil
}
func handleZmqEvents(eventString string) transaction.Transactions {
	var txSlice transaction.Transactions
	eventParams := strings.Split(eventString, " ")
	if eventParams[0] != "tx_trytes" {
		return nil
	}
	txHash = eventParams[2]
	txTrits, err := trinary.TrytesToTrits(eventParams[1])
	if err != nil {
		fmt.Println("ZMQ_LISTENER: get transactions error: ", err)
		return nil
	}
	tx, err := transaction.ParseTransaction(txTrits)
	if err != nil {
		fmt.Println("ZMQ_LISTENER: parse transactions error: ", err)
		return nil
	}
	txSlice = append(txSlice, *tx)
	return txSlice
}
func filterTransactions(transactionBufferShared *atomicTransactionBuffer) transaction.Transactions {
	var filteredTx, transactionFilter transaction.Transactions
	transactionBufferShared.retrieveTransactions(&transactionFilter)
	for _, tx := range transactions {
		filtered := false
		for _, filter := range transactionFilter {
			if tx.Hash == filter.Hash {
				fmt.Println("filtered: ", tx.Address)
				filtered = true
				break
			}
		}
		if !filtered {
			filteredTx = append(filteredTx, tx)
		}
	}
	return filteredTx
}
func synchronizeMedblocks(medblocks []medIota.Medblock) {
	var req GS.ZMQRequest
	req.Medblocks = append(req.Medblocks, medblocks...)
	reqStr, _ := json.Marshal(req)
	if reqStr != nil {
		http.Post("http://"+GS.SynchronizerEndpoint+":3000/synchronizer/addMedblock", "json/text", bytes.NewBuffer(reqStr))
	}
}

func zmqListener(transactionBufferShared *atomicTransactionBuffer) {

	err := zmqInit()
	if err != nil {
		return
	}

	for true {
		eventString, _ := socket.Recv(zmq.DONTWAIT)
		if eventString != "" {
			transactions = append(transactions, handleZmqEvents(eventString)...)
			eventString = ""
		} else if len(transactions) != 0 {
			filteredTransactions = filterTransactions(transactionBufferShared)
			medblocks = medIota.FilterAndClassifyMedblocks(filteredTransactions)
			if len(medblocks) != 0 {
				synchronizeMedblocks(medblocks)
			}
			medblocks = nil
			filteredTransactions = nil
			transactions = nil
		} else {
			time.Sleep(time.Millisecond * 100)
		}
	}
}
