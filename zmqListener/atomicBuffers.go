package main

import (
	"sync"

	"github.com/iotaledger/iota.go/transaction"
)

type atomicTransactionBuffer struct {
	mu              sync.Mutex // A lock than can be held by one goroutine at a time.
	transactions    transaction.Transactions
	newTransactions bool
}

func (buffer *atomicTransactionBuffer) appendTransactions(inputTransactions transaction.Transactions) {
	buffer.mu.Lock()
	buffer.transactions = append(buffer.transactions, inputTransactions...)
	buffer.newTransactions = true
	buffer.mu.Unlock()
}
func (buffer *atomicTransactionBuffer) retrieveTransactions(outputTransactions *transaction.Transactions) {
	buffer.mu.Lock()
	*outputTransactions = append(*outputTransactions, buffer.transactions...)
	buffer.newTransactions = false
	buffer.transactions = nil
	buffer.mu.Unlock()
}
func (buffer *atomicTransactionBuffer) checkNewTransactions() bool {
	buffer.mu.Lock()
	output := buffer.newTransactions
	buffer.mu.Unlock()
	return output
}
