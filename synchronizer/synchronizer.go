package main

import (
	"fmt"
	"time"

	"gitlab.com/medblocks/medblocks_backend/go-couchdb"

	"github.com/iotaledger/iota.go/transaction"
	GS "gitlab.com/medblocks/medblocks_backend/globalStuff"
	"gitlab.com/medblocks/medblocks_backend/medIota"
)

var zmqBuffer []medIota.Medblock
var requestBuffer []GS.RequestWrapper
var medblockList []medIota.Medblock
var sentTransactions transaction.Transactions
var clie *couchdb.Client

func processZmq(zmqBuffer []medIota.Medblock) {
	for _, medblock := range zmqBuffer {
		switch medblock.Type {
		case medIota.REGISTER:
			zmqProcessRegister(medblock)
		default:
			{
				fmt.Println("SYNCHRONIZER: Invalid medblock from ZMQ")
			}
		}
	}
}
func processRequests(requestBuffer []GS.RequestWrapper) {
	if len(requestBuffer) != 0 {
		for _, request := range requestBuffer {
			switch request.RequestType {
			case GS.NewUser:
				continue
			default:
				fmt.Println("SYNCHRONIZER: Invalid request from request handler")
			}
		}
	}
	synchronizeToIota()
}
func synchronizer(requestBufferShared *atomicRequestBuffer, zmqBufferShared *atomicMedblockBuffer) {
	var err error
	clie, err = couchdb.DBLogin("", "")
	if err != nil {
		fmt.Println("SYNCHRONIZER: DB login error: ", err)
		return
	}
	for true {
		time.Sleep(time.Second * 10)

		zmqBufferShared.retrieveMedblocks(&zmqBuffer)
		processZmq(zmqBuffer)
		zmqBuffer = nil

		requestBufferShared.retrieveRequests(&requestBuffer)
		processRequests(requestBuffer)
		requestBuffer = nil
	}
}
