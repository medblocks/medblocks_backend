package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"net/http"

	GS "gitlab.com/medblocks/medblocks_backend/globalStuff"
)

func addRequest(requestBufferShared *atomicRequestBuffer) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		var request GS.RequestWrapper
		err := json.NewDecoder(r.Body).Decode(&request)
		if err != nil {
			fmt.Println("SYNCHRONIZER: Request decode error: ", err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		if request.RequestMessage == "" || request.RequestType == GS.Invalid {
			fmt.Println("SYNCHRONIZER: Incomplete request: ")
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		requestBufferShared.appendRequest(request)
	}
}
func addMedblock(zmqBufferShared *atomicMedblockBuffer) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		var request GS.ZMQRequest
		err := json.NewDecoder(r.Body).Decode(&request)
		if err != nil {
			fmt.Println("SYNCHRONIZER: ZMQ request decode error: ", err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		if request.Medblocks == nil {
			fmt.Println("SYNCHRONIZER: Incomplete request: ")
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		zmqBufferShared.appendMedblocks(request.Medblocks)
		w.WriteHeader(http.StatusAccepted)
	}
}

func main() {
	var requestBufferShared atomicRequestBuffer
	var zmqBufferShared atomicMedblockBuffer
	docker := flag.Bool("docker", false, "pass to change endpoints to docker")
	flag.Parse()
	GS.SetEndpoints(docker)
	go synchronizer(&requestBufferShared, &zmqBufferShared)
	http.HandleFunc("/synchronizer/addRequest", addRequest(&requestBufferShared))
	http.HandleFunc("/synchronizer/addMedblock", addMedblock(&zmqBufferShared))
	http.ListenAndServe(":3000", nil)
}
