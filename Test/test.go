package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"sync"

	. "gitlab.com/medblocks/medblocks_backend/globalStructs"
)

func sendCreateUser(i int, joiner *sync.WaitGroup) {
	req := User{EmailID: "aiaaanne" + strconv.Itoa(i), PrivateKey: "yoyo", PublicKey: "hmm", Vector: "lel"}
	reqStr, _ := json.Marshal(req)
	resp, err := http.Post("http://localhost:8080/registerUser", "application/json", bytes.NewBuffer([]byte(reqStr)))
	fmt.Println(resp)
	if err != nil {
		fmt.Println(err)
	}
	joiner.Done()
}

func main() {
	var joiner sync.WaitGroup
	joiner.Add(10)
	for i := 0; i < 10; i++ {
		go sendCreateUser(4, &joiner)
	}
	joiner.Wait()
	// req := User{EmailID: "user111"}
	// reqStr, _ := json.Marshal(req)
	// resp, cErr := http.Post("http://localhost:8080/login", "application/json", bytes.NewBuffer([]byte(reqStr)))
	// body, err2 := ioutil.ReadAll(resp.Body)
	// fmt.Println(string(body), err2, cErr)
}
